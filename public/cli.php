<?php

ini_set('display_errors', TRUE);

define("BASE_PATH", dirname(dirname(__FILE__)) . '/system');
define("APPLICATION_PATH", BASE_PATH . "/application");
define("STARTTIME", microtime(TRUE));

$application = new Yaf_Application(BASE_PATH . "/conf/application.ini");

$response = $application
        ->bootstrap()
        ->getDispatcher()
        ->dispatch(new Yaf_Request_Simple());
