<?php

/**
 * Index
 */
class IndexController extends Controller {

    public function init() {
        parent::init();
    }

    public function __call($name, $arguments) {
        Tools::p(__DIR__);
        echo "<hr />模块/控制器/方法：";
        Tools::p($this->_request->getModuleName(), $this->_request->getControllerName(), $this->_request->getActionName());
        echo "<hr />请求方法：";
        Tools::p($name);
        echo "<hr />参数列表：";
        Tools::p($arguments);
        echo "<hr />Params参数：";
        Tools::p($this->_request->getParams());
        exit;
    }

}
