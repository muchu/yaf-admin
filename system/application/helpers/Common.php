<?php

function r($data = NULL) {
    $items = is_array($data) ? $data : func_get_args();
    return $items[array_rand($items)];
}
?>
<?php

function build_dropdown($options = array(), $index = 0) { ?>
    <div class="btn-group btn-filter">

        <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-index="<?= $index ?>" data-toggle="dropdown">
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li class="active"><a href="javascript:;" data-filter="">全部</a></li>
                <?php foreach ($options as $k => $v) { ?>
                <li><a href="javascript:;" data-filter="<?= $k ?>"><?= $v ?></a></li>
            <?php } ?>
        </ul>
    </div>

<?php } ?>
