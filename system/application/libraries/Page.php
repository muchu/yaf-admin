<?php

/**
 * 分页类
 */
class Page {

    protected static $_instance = null;
    public $options = array('pagesize' => 10, 'items' => 4, 'showinfo' => 0, 'wraptag' => 'li', 'prevtext' => '<', 'nexttext' => '>', 'dottedtext' => '...', 'mode' => 3);

    function __construct() {
        if (Yaf_Registry::get("config")->page) {
            $this->options = array_merge($this->options, Yaf_Registry::get("config")->page->toArray());
        }
    }

    /**
     * 单例方法
     * @return Page
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function setOption($params = array()) {
        $this->options = array_merge($this->options, $params);
    }

    /**
     * 获取分页数据
     * @param int $totals 总记录数
     * @param int $current  当前页码
     * @param int $pagesize 分页大小
     * @param string $pageurl 分页的URL地址
     * @param bool $showinfo 是否显示总记录信息
     * @return array 分页的数据信息
     */
    public function get($totals = null, $current = null, $pagesize = null, $items = null, $pageurl = null, $showinfo = null) {
        $totals = is_null($totals) ? 0 : $totals;
        $current = is_null($current) ? 1 : $current;
        $pagesize = is_null($pagesize) ? $this->options['pagesize'] : $pagesize;
        $items = is_null($items) ? $this->options['items'] : $items;
        $showinfo = is_null($showinfo) ? $this->options['showinfo'] : $showinfo;
        $from = $to = 1;
        $pages = 1;
        //判断记录总数是否大于分页大小
        if ($totals > $pagesize) {
            if (is_null($pageurl)) {
                $pageurl = Tools::getCurrentURL();
                switch ($this->options['mode']) {
                    case 0:
                        break;
                    case 1:
                        $pageurl = preg_replace('/\/[0-9]+(\/?)$/', '/{page}$1', $pageurl);
                        break;
                    case 2:
                        $pageurl = preg_replace('/\/page\/[0-9]+/', '/page/{page}', $pageurl);
                        break;
                    default:
                        $pageurl = preg_replace('/([\?|&]+)page=[0-9]+/', '$1page={page}', $pageurl);
                        break;
                }
            }
            if (stripos($pageurl, '{page}') === false) {
                switch ($this->options['mode']) {
                    case 0:
                        //为0时不做任何替换
                        break;
                    case 1:
                        //增加/1/
                        $pageurl = rtrim($pageurl, '/') . '/{page}/';
                        break;
                    case 2:
                        //增加/page/1/
                        $pageurl = rtrim($pageurl, '/') . '/page/{page}/';
                        break;
                    default:
                        //增加?page=1
                        $pageurl = $pageurl . (stripos($pageurl, '?') === false ? '?' : '&') . 'page={page}';
                        break;
                }
            }

            //获取总页数
            $pages = ceil($totals / $pagesize);

            //当总页数小于每页显示的分页数时
            if ($pages < $items)
                $to = $pages;

            $current = $current > $pages ? $pages : $current;
            //分页大于每页显示的分页数 且 当前分页大于每页显示的记录数时
            if ($current < $items) {
                $from = 1;
            } else if ($current > ($pages - $items + 1)) {
                $from = $pages - $items + 1;
            } else {
                $from = $current - ceil($items / 2) + 1;
            }
            $to = $from + $items - 1;
            $to = $to > $pages + 1 ? $pages + 1 : $to;
        }
        $ret = array(
            'pageurl' => is_null($pageurl) ? '' : $pageurl,
            'current' => $current,
            'pages' => $pages,
            'totals' => $totals,
            'from' => $from,
            'to' => $to,
            'pagesize' => $pagesize,
            'items' => $items,
            'showinfo' => $showinfo,
        );
        return array_merge($this->options, $ret);
    }

    public function build($params = array()) {
        $info = '';
        $wrap = isset($params['wraptag']) ? $params['wraptag'] : 'li';
        if (isset($params['showinfo']) && $params['showinfo']) {
            $info .= "<{$wrap} class='total'>总共 " . (isset($params['totals']) ? $params['totals'] : 0) . " 条记录</{$wrap}>";
        }
        if ($params) {
            $info .= $params['current'] == 1 ? "<{$wrap} class='disabled'><span>{$params['prevtext']}</span></{$wrap}>" : "<{$wrap}><a href=\"" . str_replace('{page}', ($params['current'] - 1), $params['pageurl']) . "\" " . $this->get_attr_result($params['current'] - 1, $params) . ">{$params['prevtext']}</a></{$wrap}>";

            if ($params['current'] >= $params['items']) {
                $info .= "<{$wrap}><a href=\"" . str_replace('{page}', 1, $params['pageurl']) . "\"" . $this->get_attr_result(1, $params) . ">1</a></{$wrap}>";
                if ($params['dottedtext']) {
                    $info .= "<{$wrap} class='dotted'><span>{$params['dottedtext']}</span></{$wrap}>";
                }
            }
            for ($params['from']; $params['from'] <= $params['to']; $params['from'] ++) {
                $info .= $params['from'] == $params['current'] ? "<{$wrap} class='active'><a href=''>{$params['from']}</a></{$wrap}>" : "<{$wrap}><a href=\"" . str_replace('{page}', $params['from'], $params['pageurl']) . "\"" . $this->get_attr_result($params['from'], $params) . ">{$params['from']}</a></{$wrap}>";
            }
            if (($params['pages'] - $params['to']) >= 1) {
                if ($params['dottedtext']) {
                    $info .= "<{$wrap} class='dotted'><span>{$params['dottedtext']}</span></{$wrap}>";
                }
                $info .= "<{$wrap}><a href=\"" . str_replace('{page}', $params['pages'], $params['pageurl']) . "\"" . $this->get_attr_result($params['pages'], $params) . ">{$params['pages']}</a></{$wrap}>";
            }
            $info .= $params['current'] == $params['pages'] ? "<{$wrap} class='disabled'><span>{$params['nexttext']}</span></{$wrap}>" : "<{$wrap}><a href=\"" . str_replace('{page}', ($params['current'] + 1), $params['pageurl']) . "\"" . $this->get_attr_result($params['current'] + 1, $params) . ">{$params['nexttext']}</a></{$wrap}>";
        }
        return $info;
    }

    function get_attr_result($page = 1, $params = array()) {
        return " totals='{$params['totals']}' pagesize='{$params['pagesize']}' page='{$page}'";
    }

}
