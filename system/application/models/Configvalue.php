<?php

/**
 * 配置模块
 * @version $Id: Configvalue.php 567 2015-02-05 11:52:51Z karson $
 */
class ConfigvalueModel Extends Model {

    public $key = "configvalue";

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct(NULL, 'content', 'name');
    }

    /**
     * 根据公式计算配置值
     * @param string $id 配置公式ID
     * @param array $var 替换来源数据
     * @param boolean $debug 是否启用调试模式
     * @return mixed
     */
    public function formula($id, $var = array(), $debug = FALSE) {
        $config = $this->get($id);
        $formulavalue = 0;
        if ($config && !is_array($config['content'])) {
            $formulavalue = $this->calculate($config['content'], $var, $debug);
        }
        return $formulavalue;
    }

    /**
     * 读取配置的值
     * @param string $id
     * @return mixed
     */
    public function values($id) {
        $config = $this->get($id);
        if ($config) {
            return json_decode($config['content'], TRUE);
        }
    }

    /**
     * 直接计算公式结果
     * @param string $formula
     * @param array $var
     * @param boolean $debug
     * @return mixed
     */
    public function calculate($formula, $var = array(), $debug = FALSE) {
        $formulavalue = 0;
        if (!is_numeric($formula)) {
            $f = '$formulavalue = ' . strtr($formula, $var) . ';';
            if ($debug) {
                Tools::d($formula, $f);
            }
            try {
                eval($f);
            } catch (Exception $e) {
                
            }
        }
        return $formulavalue;
    }

}
