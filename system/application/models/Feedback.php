<?php

/**
 * 反馈模块
 * @version $Id: Feedback.php 1348 2013-11-28 12:10:30Z zks $
 */
class FeedbackModel Extends Model {

    public $pagesize = 10;

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 读取指定ID反馈信息
     * @param string $id 反馈ID
     * @return array
     */
    public function get($id) {
        return $this->db->query("SELECT * FROM {pre}feedback WHERE id = ? LIMIT 1", array($id))->fetch();
    }

    /**
     * 读取指定账户ID和玩家ID的反馈信息
     * @param int $user_id
     * @return array
     */
    public function getByUser($user_id) {
        return $this->db->one("SELECT * FROM {pre}feedback WHERE user_id = ? ORDER BY createtime DESC LIMIT 1", array($user_id));
    }

    /**
     * 写入一条反馈信息
     * @param array $params
     * @return array 更新成功后的数据
     */
    public function create($params) {
        $params['createtime'] = time();
        $params['status'] = 'normal';
        $allowparams = array('user_id', 'type', 'content', 'createtime', 'status');
        $params = array_intersect_key($params, array_flip($allowparams));
        try {
            $this->db->create("{pre}feedback", $params);
            $params['id'] = $this->db->id();
            return $params;
        } catch (Exception $e) {
            Logger::getInstance()->error($e);
            return NULL;
        }
    }

    /**
     * 更新反馈信息
     * @param array $params 更新的数据
     * @param array $where 更新条件
     * @return boolean
     */
    public function update($params, $where) {
        return $this->db->update("{pre}feedback", $params, $where)->count() ? TRUE : FALSE;
    }

    /**
     * 读取玩家反馈列表
     * @param int $offset 偏移值ID
     * @param int $user_id 会员ID
     * @return array 列表和分页信息
     */
    public function lists($offset, $user_id) {
        $params = array($user_id, 'hidden');
        $where = "";
        if ($offset) {
            $where = " AND id < ?";
            $params[] = $offset;
        }
        $list = $this->db->all("SELECT * FROM {pre}feedback WHERE user_id = ? AND status != ? {$where} ORDER BY id DESC LIMIT {$this->pagesize}", $params);
        return array_values($list);
    }

    /**
     * 删除指定反馈信息
     * @param string $id 反馈ID
     * @param string $user_id 玩家ID
     * @return boolean
     */
    public function delete($id, $user_id) {
        return $this->db->query("DELETE {pre}feedback WHERE id = ? AND user_id = ?", array($id, $user_id))->count() ? TRUE : FALSE;
    }

}
