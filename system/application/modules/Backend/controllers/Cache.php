<?php

class CacheController extends Backend {

    public function init() {
        parent::init();
        $this->_view->assign(array('title' => '缓存', 'nickname' => 'Cache', 'intro' => '删除清除缓存等操作'));
    }

    public function indexAction() {
        if ($this->_request->getQuery("draw")) {
            $total = 10000;
            $columns = $this->_request->getQuery("columns");
            $order = $this->_request->getQuery("order");
            $search = $this->_request->getQuery("search");
            $wherearr = array('1=1');
            $orderarr = array('id,DESC');
            $params = array();
            $where = implode(' AND ', $wherearr);
            $orderby = implode(',', $orderarr);
            $draw = $this->_request->getQuery("draw");
            $start = $this->_request->getQuery("start");
            $length = $this->_request->getQuery("length");

            $data = array();
            $typearr = array(
                1 => 'string', 2 => 'set', 3 => 'list', 4 => 'zset', 5 => 'hash', 6 => 'other'
            );
            foreach ($this->redis->keys("*") as $m => $n) {
                if ($search && $search['value']) {
                    if (stripos($n, $search['value']) === FALSE)
                        continue;
                }
                $key = str_replace(array($this->config['cache']['prefix']), '', $n);
                $name = $key;
                $type = $this->redis->type($key);
                $expiretime = $this->redis->ttl($key);
                $expiretime = $expiretime == -1 ? '永不过期' : date('Y-m-d H:i:s', $expiretime + time());
                $data[] = array('DT_RowId' => $key, 'id' => $key, 'index' => $m, 'name' => $name, 'type' => isset($typearr[$type]) ? $typearr[$type] : $typearr[6], 'expiretime' => $expiretime);
            }
            $data = Tools::getArraySort($data, 'id', 'SORT_ASC');
            $data = array_slice($data, $start, $length);
            foreach ($data as $k => & $v) {
                $v['index'] = $start + $k + 1;
            }
            $result = array("draw" => $draw, "recordsTotal" => $total, "recordsFiltered" => $total, "data" => $data);

            echo json_encode($result);
            return FALSE;
        }
    }

    public function addAction() {
        return FALSE;
    }

    public function editAction($ids = NULL) {
        return FALSE;
    }

    public function delAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            $idsarr = explode(',', $ids);
            foreach ($idsarr as $k => $v) {
                $this->cache->delete($v);
            }
            if (TRUE) {
                $code = 0;
                $content = "操作成功！共删除" . count($idsarr) . "条数据！";
            } else {
                $content = "操作失败！共删除0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

    public function multiAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            parse_str($this->_request->getPost("params"), $values);
            if ($values) {
                $this->db->update("{pre}cache", $values, "id IN ({$ids})");
                $count = $this->db->count();
                if ($count) {
                    $code = 0;
                    $content = "操作成功！共更新{$count}条数据！";
                } else {
                    $content = "操作失败！共更新0条数据！";
                }
            } else {
                $content = "操作失败！共更新0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

}
