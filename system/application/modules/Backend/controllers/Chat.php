<?php

class ChatController extends Backend {

    public function init() {
        parent::init();
        $this->_view->assign(array('title' => '客服聊天', 'nickname' => 'Chat', 'intro' => '查看聊天、回复等操作'));
        $this->load->model("form");
        $this->form->senderrolearr = array('user' => '<font color="red">未回复</font>', 'admin' => '<font color="black">已回复</font>');
    }

    public function indexAction() {
        if ($this->_request->getQuery("draw")) {
            $total = 10000;
            $columns = $this->_request->getQuery("columns");
            $order = $this->_request->getQuery("order");
            $search = $this->_request->getQuery("search");
            $wherearr = array('1=1');
            $orderarr = array('id,DESC');
            $params = array();
            foreach ($columns as $k => $v) {
                if ($v['search'] && $v['search']['value']) {
                    $wherearr[] = "FIND_IN_SET({$v['data']}, ?)";
                    $params[] = $v['search']['value'];
                }
            }
            if ($search && $search['value']) {
                $wherearr[] = "(id LIKE ? OR user_id LIKE ? OR sender_role LIKE ? OR content LIKE ?)";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
            }
            if ($order) {
                $orderarr = array();
                foreach ($order as $k => $v) {
                    $orderarr[] = "{$columns[$v['column']]['data']} {$v['dir']}";
                }
            }
            $where = implode(' AND ', $wherearr);
            $orderby = implode(',', $orderarr);
            $draw = $this->_request->getQuery("draw");
            $start = $this->_request->getQuery("start");
            $length = $this->_request->getQuery("length");
            $data = array();
            $this->db->query("SELECT t.* FROM (SELECT * FROM {pre}chat WHERE {$where} ORDER BY id DESC LIMIT {$start}, {$length}) t GROUP BY t.user_id", $params);
            while ($row = $this->db->fetch()) {
                $row['DT_RowId'] = $row['id'];
                $row['readtime'] = Tools::getDateTime($row['readtime']);
                $row['createtime'] = Tools::getDateTime($row['createtime']);
                $row['content'] = Tools::cutstr($row['content'], 6, '...');
                $row['status'] = isset($this->form->statusarr[$row['status']]) ? $this->form->statusarr[$row['status']] : "未知[{$row['status']}]";
                $data[] = $row;
            }
            $result = array("draw" => $draw, "recordsTotal" => $total, "recordsFiltered" => $total, "data" => $data);

            echo json_encode($result);
            return FALSE;
        }
        $this->_view->assign("senderrolelist", $this->form->senderrolearr);
    }

    public function addAction() {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            $params['readtime'] = time();
            $params['createtime'] = time();
            $params['status'] = 'normal';
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            if (!$params['content']) {
                echo json_encode(array('code' => 1000, 'content' => ''));
                exit;
            }
            if ($params) {
                $this->db->insert("{pre}chat", $params);
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        return false;
    }

    public function fetchAction($user_ids = NULL) {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $row = $this->db->all("SELECT * FROM {pre}chat WHERE user_id = ?", array($user_ids));
        $this->_view->assign("row", $row);
        $this->_view->assign("user_ids", $user_ids);
    }

    public function editAction($ids = NULL) {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            $params['sender_role'] = 'admin';
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            if ($params) {
                $this->db->update("{pre}chat", $params, array('id' => $ids));
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $row = $this->db->one("SELECT * FROM {pre}chat WHERE id = ?", array($ids));
        $this->_view->assign("row", $row);
    }

    public function delAction($ids = NULL) {
        $code = -1;
        $content = '';
        if ($ids) {
            $this->db->delete("{pre}chat", "user_id IN ({$ids})");
            $count = $this->db->count();
            if ($count) {
                $code = 0;
                $content = "操作成功！共删除{$count}条数据！";
            } else {
                $content = "操作失败！共删除0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

    public function multiAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            parse_str($this->_request->getPost("params"), $values);
            if ($values) {
                $this->db->update("{pre}chat", $values, "id!=1 AND id IN ({$ids})");
                $count = $this->db->count();
                if ($count) {
                    $code = 0;
                    $content = "操作成功！共更新{$count}条数据！";
                } else {
                    $content = "操作失败！共更新0条数据！";
                }
            } else {
                $content = "操作失败！共更新0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

}
