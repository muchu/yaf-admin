<?php

class DistanceController extends Backend {

    public function init() {
        parent::init();
        $this->_view->assign(array('title' => '里程', 'nickname' => 'Distance', 'intro' => '查看里程、编辑、删除等操作'));
        $this->load->model("form");
    }

    public function indexAction() {
        if ($this->_request->getQuery("draw")) {
            $total = 10000;
            $columns = $this->_request->getQuery("columns");
            $order = $this->_request->getQuery("order");
            $search = $this->_request->getQuery("search");
            $wherearr = array('1=1');
            $orderarr = array('id,DESC');
            $params = array();
            foreach ($columns as $k => $v) {
                if ($v['search'] && $v['search']['value']) {
                    $wherearr[] = "FIND_IN_SET({$v['data']}, ?)";
                    $params[] = $v['search']['value'];
                }
            }
            if ($search && $search['value']) {
                $wherearr[] = "(id LIKE ? OR user_id LIKE ? OR car_mac LIKE ?)";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
            }
            if ($order) {
                $orderarr = array();
                foreach ($order as $k => $v) {
                    $orderarr[] = "{$columns[$v['column']]['data']} {$v['dir']}";
                }
            }
            $where = implode(' AND ', $wherearr);
            $orderby = implode(',', $orderarr);
            $draw = $this->_request->getQuery("draw");
            $start = $this->_request->getQuery("start");
            $length = $this->_request->getQuery("length");
            $data = array();
            $this->db->query("SELECT t.* FROM (SELECT * FROM {pre}drive WHERE {$where} ORDER BY {$orderby} LIMIT {$start}, {$length}) t GROUP BY t.user_id", $params);
            while ($row = $this->db->fetch()) {
                $row['DT_RowId'] = $row['id'];
//                $row['createtime'] = Tools::getDateTime($row['createtime']);
//                $row['updatetime'] = Tools::getDateTime($row['updatetime']);
                $row['status'] = isset($this->form->statusarr[$row['status']]) ? $this->form->statusarr[$row['status']] : "未知[{$row['status']}]";
                $data[] = $row;
            }
            $result = array("draw" => $draw, "recordsTotal" => $total, "recordsFiltered" => $total, "data" => $data);

            echo json_encode($result);
            return FALSE;
        }
        $this->_view->assign("statuslist", $this->form->statusarr);
    }

    public function addAction() {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            if ($params) {
                $this->db->insert("{pre}drive", $params);
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
    }

    public function editAction($ids = NULL) {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("v");
            foreach ($params as $k => & $v) {
                $k = is_array($k) ? Tools::encode($k) : $k;
            }
            if ($params) {
                $this->db->update("{pre}drive", $params, array('id' => $ids));
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $data = array();
        $row = $this->db->query("SELECT d.*, c.bluetoothname FROM {pre}drive d, {pre}car c WHERE d.user_id = ? AND d.car_mac=c.mac ORDER BY d.id DESC", array($ids));
        while ($row = $this->db->fetch()) {
//            $row['createtime'] = Tools::getDateTime($row['createtime']);
//            $row['updatetime'] = Tools::getDateTime($row['updatetime']);
            $row['status'] = isset($this->form->statusarr[$row['status']]) ? $this->form->statusarr[$row['status']] : "未知[{$row['status']}]";
            $data[] = $row;
        }
        $this->_view->assign("row", $data);
        $this->_view->assign("user_ids", $ids);
    }

    public function delAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            $this->db->delete("{pre}drive", "id IN ({$ids})");
            $count = $this->db->count();
            if ($count) {
                $code = 0;
                $content = "操作成功！共删除{$count}条数据！";
            } else {
                $content = "操作失败！共删除0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

    public function multiAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            parse_str($this->_request->getPost("params"), $values);
            if ($values) {
                $this->db->update("{pre}drive", $values, "id!=1 AND id IN ({$ids})");
                $count = $this->db->count();
                if ($count) {
                    $code = 0;
                    $content = "操作成功！共更新{$count}条数据！";
                } else {
                    $content = "操作失败！共更新0条数据！";
                }
            } else {
                $content = "操作失败！共更新0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

}
