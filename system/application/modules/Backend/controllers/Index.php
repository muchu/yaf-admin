<?php

class IndexController extends Backend {

    public function init() {
        parent::init();
        $this->_view->assign(array('title' => '控制台', 'nickname' => 'Dashboard', 'intro' => '控制台统计相关信息'));
    }

    public function indexAction() {
        
    }

    public function loginAction() {
        if ($this->auth->is_login()) {
            Tools::info("你已经登录，无需重复登录", Tools::siteurl("/"));
        }
        if ($this->_request->isPost()) {
            $username = $this->_request->getRequest("username");
            $password = $this->_request->getRequest("password");
            if ($username && $password) {
                $loginret = $this->auth->login($username, $password);
                if ($loginret) {
                    //Tools::success("登录成功", Tools::siteurl('index/index'));
                    header("location:" . Tools::siteurl('index/index'));
                } else {
                    Tools::error($this->auth->error);
                }
            } else {
                Tools::error("用户名密码不能为空");
            }
        }
    }

    public function logoutAction() {
        $this->auth->logout();
        Tools::success("退出成功", Tools::siteurl('index/login'));
    }

}
