<?php

class PageController extends Backend {

    private $categoryarr = array();

    public function init() {
        parent::init();
        $this->load->model("form");
        $this->_view->assign(array('title' => '单页', 'nickname' => 'Page', 'intro' => '新增、编辑、删除单页，自定义单页面等操作'));
    }

    public function indexAction() {
        if ($this->_request->getQuery("draw")) {
            $total = 10000;
            $columns = $this->_request->getQuery("columns");
            $order = $this->_request->getQuery("order");
            $search = $this->_request->getQuery("search");
            $wherearr = array('1=1');
            $orderarr = array('id,DESC');
            $params = array();
            foreach ($columns as $k => $v) {
                if ($v['search'] && $v['search']['value']) {
                    $wherearr[] = "FIND_IN_SET({$v['data']}, ?)";
                    $params[] = $v['search']['value'];
                }
            }
            if ($search && $search['value']) {
                $wherearr[] = "(title LIKE ? OR diyname LIKE ?)";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
            }
            if ($order) {
                $orderarr = array();
                foreach ($order as $k => $v) {
                    $orderarr[] = "{$columns[$v['column']]['data']} {$v['dir']}";
                }
            }
            $where = implode(' AND ', $wherearr);
            $orderby = implode(',', $orderarr);
            $draw = $this->_request->getQuery("draw");
            $start = $this->_request->getQuery("start");
            $length = $this->_request->getQuery("length");
            $data = array();
            $this->db->query("SELECT * FROM {pre}page WHERE {$where} ORDER BY {$orderby} LIMIT {$start}, {$length}", $params);
            while ($row = $this->db->fetch()) {
                $row['DT_RowId'] = $row['id'];
                $category_id = $row['category_id'];
                $categoryinfo = $this->category->get($category_id);
                $row['categoryname'] = $categoryinfo ? $categoryinfo['name'] : "未知[{$category_id}]";
                $flagarr = array();
                foreach (explode(',', $row['flag']) as $k => $v) {
                    $flagarr[] = isset($this->form->flagarr[$v]) ? $this->form->flagarr[$v] : "未知[{$v}]";
                }
                $row['flag'] = implode(',', $flagarr);
                $row['publishtime'] = date("Y-m-d", $row['publishtime']);
                $row['status'] = isset($this->form->statusarr[$row['status']]) ? $this->form->statusarr[$row['status']] : "未知[{$row['status']}]";
                $data[] = $row;
            }
            $result = array("draw" => $draw, "recordsTotal" => $total, "recordsFiltered" => $total, "data" => $data);

            echo json_encode($result);
            return FALSE;
        }
        $this->_view->assign("flaglist", $this->form->flagarr);
        $this->_view->assign("statuslist", $this->form->statusarr);
    }

    public function addAction() {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = substr($k, -4) == 'time' && !is_numeric($v) ? strtotime($v) : $v;
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            $params['createtime'] = time();
            $params['updatetime'] = time();
            if ($params) {
                $this->db->insert("{pre}page", $params);
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $this->_view->assign("categoryoptions", $this->form->categoryoptions(2));
        $this->_view->assign("flagoptions", $this->form->flagoptions());
    }

    public function editAction($ids = NULL) {
        $row = $this->db->one("SELECT * FROM {pre}page WHERE id = ?", array($ids));
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = substr($k, -4) == 'time' && !is_numeric($v) ? strtotime($v) : $v;
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            unset($v);
            $params['updatetime'] = time();
            if ($params) {
                $this->db->update("{pre}page", $params, array('id' => $ids));
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $this->_view->assign("row", $row);
        $this->_view->assign("categoryoptions", $this->form->categoryoptions(2, $row['category_id']));
        $this->_view->assign("flagoptions", $this->form->flagoptions($row['flag']));
    }

    public function delAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            $this->db->delete("{pre}page", "id IN ({$ids})");
            $count = $this->db->count();
            if ($count) {
                $code = 0;
                $content = "操作成功！共删除{$count}条数据！";
            } else {
                $content = "操作失败！共删除0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

    public function multiAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            parse_str($this->_request->getPost("params"), $values);
            if ($values) {
                $this->db->update("{pre}page", $values, "id IN ({$ids})");
                $count = $this->db->count();
                if ($count) {
                    $code = 0;
                    $content = "操作成功！共更新{$count}条数据！";
                } else {
                    $content = "操作失败！共更新0条数据！";
                }
            } else {
                $content = "操作失败！共更新0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

}
