<?php

class UserController extends Backend {

    private $parentarr = array();
    private $genderarr = array();

    public function init() {
        parent::init();
        $this->_view->assign(array('title' => '会员', 'nickname' => 'User', 'intro' => '编辑会员、删除等操作'));
        $this->load->model("form");
        $this->genderarr = array('-1' => '请选择性别', '1' => '男', '0' => '女');
    }

    public function indexAction() {
        if ($this->_request->getQuery("draw")) {
            $total = 10000;
            $columns = $this->_request->getQuery("columns");
            $order = $this->_request->getQuery("order");
            $search = $this->_request->getQuery("search");
            $wherearr = array('1=1');
            $orderarr = array('id DESC');
            $params = array();
            foreach ($columns as $k => $v) {
                if ($v['search'] && $v['search']['value']) {
                    $wherearr[] = "FIND_IN_SET({$v['data']}, ?)";
                    $params[] = $v['search']['value'];
                }
            }
            if ($search && $search['value']) {
                $wherearr[] = "(username LIKE ? OR nickname LIKE ? OR email LIKE ? OR gender LIKE ? OR countrycode LIKE ? OR cellphone LIKE ? OR avatar LIKE ? OR logintime LIKE ? OR prevtime LIKE ? OR status LIKE ?)";
               
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
            }
            if ($order) {
                $orderarr = array();
                foreach ($order as $k => $v) {
                    $orderarr[] = "{$columns[$v['column']]['data']} {$v['dir']}";
                }
            }
            $where = implode(' AND ', $wherearr);
            $orderby = implode(',', $orderarr);
            $draw = $this->_request->getQuery("draw");
            $start = $this->_request->getQuery("start");
            $length = $this->_request->getQuery("length");
            $data = array();
            $this->db->query("SELECT * FROM {pre}user WHERE {$where} ORDER BY {$orderby} LIMIT {$start}, {$length}", $params);
            while ($row = $this->db->fetch()) {
                $row['DT_RowId'] = $row['id'];
                $row['status'] = isset($this->form->statusarr[$row['status']]) ? $this->form->statusarr[$row['status']] : "未知[{$row['status']}]";
                $row['gender'] = isset($row['gender']) ? ($row['gender'] == '1' ? '男' : ($row['gender'] == '0' ? '女' : '未知')) : "未知";
                $row['logintime'] = isset($row['logintime']) ? Tools::getDateTime($row['logintime'], true, false) : "未知";
                $row['prevtime'] = isset($row['prevtime']) ? Tools::getDateTime($row['prevtime'], true, false) : "未知";
                $data[] = $row;
            }
            $result = array("draw" => $draw, "recordsTotal" => $total, "recordsFiltered" => $total, "data" => $data);

            echo json_encode($result);
            return FALSE;
        }
        $this->_view->assign("statuslist", $this->form->statusarr);
    }

    public function addAction() {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            $params['salt'] = Tools::getRandNum(4);
            $params['password'] = md5(md5($params['password']) . $params['salt']);
            if ($params) {
                $this->db->insert("{pre}user", $params);
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $this->_view->assign("genderoptions", $this->form->options($this->genderarr));
    }

    public function editAction($ids = NULL) {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            //如果是1则移除级别和状态
            if ($ids == '1') {
                unset($params['status']);
            }
            if ($params['password']) {
                $params['salt'] = Tools::getRandNum(4);
                $params['password'] = md5(md5($params['password']) . $params['salt']);
            } else {
                unset($params['password']);
            }
            if ($params) {
                $this->db->update("{pre}user", $params, array('id' => $ids));
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $row = $this->db->one("SELECT * FROM {pre}user WHERE id = ?", array($ids));
        $this->_view->assign("row", $row);
        $this->_view->assign("genderoptions", $this->form->options($this->genderarr, $row['gender']));
    }

    public function delAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            $this->db->delete("{pre}user", "id IN ({$ids})");
            $count = $this->db->count();
            if ($count) {
                $code = 0;
                $content = "操作成功！共删除{$count}条数据！";
            } else {
                $content = "操作失败！共删除0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

    public function multiAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            parse_str($this->_request->getPost("params"), $values);
            if ($values) {
                $this->db->update("{pre}user", $values, "id!=1 AND id IN ({$ids})");
                $count = $this->db->count();
                if ($count) {
                    $code = 0;
                    $content = "操作成功！共更新{$count}条数据！";
                } else {
                    $content = "操作失败！共更新0条数据！";
                }
            } else {
                $content = "操作失败！共更新0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

}
